function [ EDGES_TO_REMOVE, P_VALUE_MAX ] = whichEdgeToRemove_Err1( CUR_A, CUR_B, CUR_C, EDGE_INDEX, P_VALUE_MAX, EXIST_FLAG, NON_PROTECTED_FLAG, ...
    CondIndepTest, alpha )

cur_pmax = P_VALUE_MAX( EDGE_INDEX );
pmax = CondIndepTest( CUR_A, CUR_B, CUR_C );

EDGES_TO_REMOVE = [];
if pmax > cur_pmax
    P_VALUE_MAX( EDGE_INDEX ) = pmax;
    if pmax > alpha
        EDGES_TO_REMOVE = EDGE_INDEX;
    end
end

end % Main Function
