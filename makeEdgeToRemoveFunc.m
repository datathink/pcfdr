function f = makeEdgeToRemoveFunc( Method, varargin )

whichEdge2Rm_interface = 'CUR_A, CUR_B, CUR_C, EDGE_INDEX, P_VALUE_MAX, EXIST_FLAG, NON_PROTECTED_FLAG';
f_gen_str = strcat( '(', ...
            ' @( ', whichEdge2Rm_interface, ' )', ...
            ' %s(', whichEdge2Rm_interface, ', %s', ' )', ...
            ' )' ...
    );

switch( Method )
    case 'FDR'
        opt_cfg = { 'indep_test', []; ...
                    'fdr_func', ( @( p ) fdr_benjamini1995( p, 0.05 ) ) ...
            };
        [ opt, varargin_rem ] = parse_opt( varargin, opt_cfg );
        if isempty( opt.indep_test )
            error( 'value of option ''indep_test'' is not given.' );
        end
        if 0 % to help depfun to discover function dependency
            whichEdgeToRemove_FDR;
        end
        f = eval( sprintf( f_gen_str, 'whichEdgeToRemove_FDR', 'opt.indep_test, opt.fdr_func, varargin_rem{ : }' ) );
        
	case 'FDRheur'
        opt_cfg = { 'indep_test', []; ...
                    'fdr_func', ( @( p ) fdr_benjamini1995( p, 0.05 ) ) ...
            };
        [ opt, varargin_rem ] = parse_opt( varargin, opt_cfg );
        if isempty( opt.indep_test )
            error( 'value of option ''indep_test'' is not given.' );
        end
        if 0 % to help depfun to discover function dependency
            whichEdgeToRemove_FDRheur;
        end        
        f = eval( sprintf( f_gen_str, 'whichEdgeToRemove_FDRheur', 'opt.indep_test, opt.fdr_func, varargin_rem{ : }' ) );
        
	case 'Err1'
        opt_cfg = { 'indep_test', []; ...
                    'err1', 0.05 ...
            };
        [ opt, varargin_rem ] = parse_opt( varargin, opt_cfg );
        if isempty( opt.indep_test )
            error( 'value of option ''indep_test'' is not given.' );
        end
        if 0 % to help depfun to discover function dependency
            whichEdgeToRemove_Err1;
        end           
        f = eval( sprintf( f_gen_str, 'whichEdgeToRemove_Err1', 'opt.indep_test, opt.err1, varargin_rem{ : }' ) );

    otherwise
end

end