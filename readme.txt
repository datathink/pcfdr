	PCfdr: FDR-controlled PC algorithm		

If this file does not display correctly with Notepad on Windows, please use WordPad instead.

------------ Introduction -------------------

This is a Matlab package for learning the skeleton of Bayesian networks using the PC algorithm, with the False Discovery Rate (FDR) controlled. It is an implementation of the algorithms published in journal paper "Controlling the False Discovery Rate of the Association/Causality Structure Learned with the PC Algorithm", Journal of Machine Learning Research, by Junning Li and Z. Jane Wang (2009).

------------ License -----------------------

    PCfdr is free software: you can redistribute it and/or modify
    it under the terms of version 3 of the GNU General Public License as published by
    the Free Software Foundation.

    PCfdr is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License (the file license.txt)
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


Please cite the two papers listed below, if you use the package in your publications
%   [JMLR2009]
%   Junning Li, Z. Jane Wang (2009)
%   "Controlling the False Discovery Rate of the Association/Causality Structure Learned with the PC Algorithm"
%   Journal of Machine Learning Research 10: 475-514
%
%   [EMBC2008]
%   Junning Li, Z. Jane Wang and Martin J. McKeown (2008)
%   "Learning brain connectivity with the false-discovery-rate-controlled PC-algorithm"
%   Proceedings of the 30th Annual International Conference of the IEEE
%   Engineering in Medicine and Biology Society. 4617 - 4620


------------ Installation -------------------

Add the folder containing the code into Matlab's path. For example, if the code is in folder /home/me/matlab/PCfdr, then type in the Matlab console:
	addpath /home/me/matlab/PCfdr


------------ Using the package --------------

learn_struct_PC, is the gateway function of the package. For instructions of this function, type in Matlab console:
	help learn_struct_PC

The package is designed to be modular, so that users can plug-in their customized components,
with options:
	"indep_test_func", and
	"fdr_func".
Please refer to the help of learn_struct_PC for more details.


------------ Uninstallation -----------------
Simply remove the folder containing the code


------------ Revision History ---------------
2009-03-03: The first release, as used in publications:
%   [JMLR2009]
%   Junning Li, Z. Jane Wang (2009)
%   "Controlling the False Discovery Rate of the Association/Causality Structure Learned with the PC Algorithm"
%   Journal of Machine Learning Research
%
%   [EMBC2008]
%   Junning Li, Z. Jane Wang and Martin J. McKeown (2008)
%   "Learning brain connectivity with the false-discovery-rate-controlled PC-algorithm"
%   Proceedings of the 30th Annual International Conference of the IEEE
%   Engineering in Medicine and Biology Society. 4617 - 4620

--------------- Credit --------------
DataThink: implemented the 2009-03-03 release

--------------- Contact --------------
datathink@users.sourceforge.net
