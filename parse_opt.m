function [ opt_out, opt_rem ] = parse_opt( opt_in, opt_cfg )
% PARSE_OPT parse options in varargin
% Syntax:
%   [ opt_out, opt_rem ] = parse_opt( opt_in, opt_cfg )
%
% Input:
%   opt_in: the input options. a cell vector. usually is the varargin in a
%   function. opt_in must has the format: 'name1', value1, 'name2',
%   value2 ...
%
%   opt_cfg: the configuration of the options. a n-by-2 cell matrix. the
%   first column is the names of the options and the second column is the
%   default value of the options.
%
% Output:
%   opt_out: the parsed options. a structure. opt_out.name1 is the value of
%   option name1.
%
%   opt_rem: the options remaining not parsed. a cell vector as opt_in.
%   Read the example below to see how to use opt_rem
%
% Example:
%   function x = fun( varargin )
%   opt_cfg1 = { 'size', 1; 'type', 'gaussian' };
%   opt_cfg2 = { 'var', 1; 'mean', 0 };
%   [ opt1, varargin ] = parse_opt( varargin, opt_cfg1 );
%   [ opt2, varargin ] = parse_opt( varargin, opt_cfg2 );
%
%
%	Junning Li, 2008-12-04
%	junningl@ece.ubc.ca

n = length( opt_in );
rem_flag = false( 1, n );

for i = 1 : 2 : n
    if ischar( opt_in{ i } )
        opt_idx = strmatch( opt_in{ i }, opt_cfg( :, 1 ) );
        if isempty( opt_idx ) % not in opt_cfg
            warning( 'ParseOpt:OptNotRecognized', 'Option %s is not recognized.', opt_in{ i } );
            rem_flag( i : i + 1 ) = true;
        else    % recognized
            opt_cfg( opt_idx( 1 ), 2 ) = opt_in( i + 1 ); % opt_idx may be a vector, so use the first one opt_idx( 1 )
        end
    else    % opt_in{ i } is not a char
        warning( 'ParseOpt:OptNameNotChar', 'The option name opt_in(%d) is not a char so this option is ignored.', i );
        rem_flag( i : i + 1 ) = true;
    end
end

if i == n % n is a odd number
    warning( 'ParseOpt:OddOptIn', 'opt_in has a odd number of elements. The last element is not parsed.' );
    rem_flag( i ) = true;
end

opt_out = cell2struct( opt_cfg( :, 2 ), opt_cfg( :, 1 ), 1 );

if nargout > 1
    opt_rem = opt_in( rem_flag );
end
