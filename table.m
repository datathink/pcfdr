function [ t, l ] = table( x, l )
% Table, Create contigency table
%   t = table( x ), create a contigency table t from data x. x is a factor 
%   matrix whose rows are observations and columns are variables. x can
%   only contain integer, logical or character values. The dimension of t is
%   the same as the number of columns of x.
%
%   [ t, l ] = table( x ). l is the level name of the factors. l is a cell.
%   the level name of t( i, j ) is [ l{ 1 }( i ) l{ 2 }( j ) ].
%
%   [ t, l ] = table( x, l ). Create the table according to the given level
%   name l. l is a cell. If l has an empty empty, the correponding level
%   name will built from x.

% Future work:
%   (1) support numeric values

nc = size( x, 2 );
if nargin < 2 || isempty( l )
    l = cell( nc, 1 );
elseif isvector( l )
    l = repmat( { l }, nc, 1 );
end

x_orig = x;
x = zeros( size( x_orig ) );
for i = 1 : nc
    if isempty( l{ i } )
        [ l{ i }, dmy, x( :, i ) ] = unique( x_orig( :, i ) );    % dmy is a dummy variable.
    else
        [ dmy, x( :, i ) ] = ismember( x_orig( :, i ), l{ i } );
    end
end

v = all( x, 2 );
if ~all( v )
    warning( 'ObservationRemoved', 'Some observations are removed because they contain unrecogined level names.' );
    x = x( v, :  );
end
nr = size( x, 1 );

s = cellfun( 'length', l );
if length( s ) < 2
    t = zeros( s, 1 );
else
    s = s( : )';
    t = zeros( s );
end

for i = 1 : nr
    ind = sub2ind( s, x( i, : ) );
    t( ind ) = t( ind ) + 1;
end

function ind = sub2ind( siz, sub )
siz = [ 1, siz( 1 : end - 1 ) ]; siz = cumprod( siz );
sub = sub - 1;
ind = sum( sub .* siz ) + 1;