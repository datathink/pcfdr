function [ p, s, df ] = chi2test( x )
% CHI2TEST Chi-square test
%
%   p = chi2test( X )
%
%   X is a contingency table

r = sum( x, 2 ); rp = r > 0; r = r( rp );
c = sum( x, 1 ); cp = c > 0; c = c( cp );
a = sum( r );
xe = r * c ./ a;
xd = x( rp, cp ) - xe;
df = length( r ) * length( c ) - length( r ) - length( c ) + 1;
s = sum( sum( ( xd .* xd ) ./ xe ) );   % the chi2 statistic
p = 1 - chi2cdf( s, df );               % the p value
