function [ t, s ] = merge3DConTable_recursive( t, m ) % Main Function
if nargin < 2
    m = 2;
end

s = squeeze( sum( sum( t, 1 ), 2 ) );

[ s, si ] = sort( s );
t = t( :, :, si );

while s( 1 ) < m && length( s ) > 2
    if s( 2 ) >= m
        t = cat( 3, t( :, :, 1 ) + t( :, :, 2 ), t( :, :, 3 : end ) );
        s = [ s( 1 ) + s( 2 ); s( 3 : end ) ];
    else
        b = find( s < m, 1, 'last' );
        idx = true( 1, length( s ) );
        idx( 1 ) = false;
        idx( b ) = false;
        t = cat( 3, t( :, :, 1 ) + t( :, :, b ), t( :, :, idx ) );
        s = [ s( 1 ) + s( b ); s( idx ) ];
    end
    
    [ s, si ] = sort( s );
    t = t( :, :, si );
end


end % Main Function
