function [ EDGES_TO_REMOVE, P_VALUE_MAX ] = whichEdgeToRemove_FDR( CUR_A, CUR_B, CUR_C, EDGE_INDEX, P_VALUE_MAX, EXIST_FLAG, NON_PROTECTED_FLAG, ...
    CondIndepTest, FDR_Func )

cur_pmax = P_VALUE_MAX( EDGE_INDEX );
pmax = CondIndepTest( CUR_A, CUR_B, CUR_C );

EDGES_TO_REMOVE = [];
if pmax > cur_pmax
    P_VALUE_MAX( EDGE_INDEX ) = pmax;
    if all( P_VALUE_MAX >= 0 )
        testing_edge = find( NON_PROTECTED_FLAG );        
        h = FDR_Func( P_VALUE_MAX( testing_edge ) );
        EDGES_TO_REMOVE = testing_edge( ~h );
    end
end

end % Main Function