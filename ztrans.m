function z = ztrans( x )
z = 0.5 .* log( ( 1 + x ) ./ ( 1 - x ) );
end