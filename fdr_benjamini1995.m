function y = fdr_benjamini1995( p, Q )
% Reference:
% Benjamini and Hochberge, "Controlling the False Discovery Rate:
%   a Practical and Powerful Approach to Multiple Testing",
%   1995, J. R. Statist. Soc. B, vol. 57, pg. 289-300
%
% q = fdr_benjamini1995( p )
% h = fdr_benjamini1995( p, Q )

m = length( p );
[ p_st, st_idx ] = sort( p );
q_st = p_st .* m ./ reshape( 1 : m, size( p_st ) );
for i = m - 1 : -1 : 1
    q_st( i ) = min( q_st( i ), q_st( i + 1 ) );
end
q = zeros( size( q_st ) );
q( st_idx ) = q_st;

if nargin < 2
    y = q;
else
    y = q <= Q;
end

end % Main Function