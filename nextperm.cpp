#include "matrix.h"
#include "mex.h"
#include <algorithm>

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
	int s[ 2 ], k;
    bool b; 
    
	s[ 0 ] = mxGetM( prhs[ 0 ] );
	s[ 1 ] = mxGetN( prhs[ 0 ] );
	k = s[ 0 ] * s[ 1 ];
    
    if ( mxIsDouble( prhs[ 0 ] ) ) {
        double *x, *y;
        plhs[ 0 ] = mxCreateDoubleMatrix( s[ 0 ], s[ 1 ], mxREAL );        
        x = mxGetPr( prhs[ 0 ] );
        y = mxGetPr( plhs[ 0 ] );
        std::copy( x, x + k, y );
        b = std::next_permutation( y, y + k );
    }
    else if ( mxIsChar( prhs[ 0 ] ) ) {
        mxChar *x, *y;
        plhs[ 0 ] = mxCreateCharArray( 2, s );
        x = mxGetChars( prhs[ 0 ] );
        y = mxGetChars( plhs[ 0 ] );
        std::copy( x, x + k, y );
        b = std::next_permutation( y, y + k );
    }
    else
         mexErrMsgIdAndTxt( "NextPerm:WrongType", "Input is neight double nor char." );

	if ( nlhs > 1 ) {
		plhs[ 1 ] = mxCreateLogicalMatrix( 1, 1 );
		mxLogical *z;
		z = mxGetLogicals( plhs[ 1 ] );
		z[ 0 ] = b;
	}
}
