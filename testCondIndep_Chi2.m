function [ p, chi2, df ] = testCondIndep_Chi2( t )
K = size( t, 3 );

p = zeros( K, 1 );
chi2 = zeros( K, 1 );
df = zeros( K, 1 );

for k = 1 : K
    [ p( k ), chi2( k ), df( k ) ] = chi2test( t( :, :, k ) );
end

chi2 = sum( chi2 );
df = sum( df );
p = 1 - chi2cdf( chi2, df );

end