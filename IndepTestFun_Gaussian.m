function x = IndepTestFun_Gaussian( a, b, c, Sigma, n, alpha ) % Main function

    Cur_Sigma = [ Sigma( a, a ) Sigma( a, c ); ...
                  Sigma( c, a ) Sigma( c, c ) ...
        ];
    %[ aa ac
    %  ca cc ]
    det_ac_ac = det( Cur_Sigma );
    
    Cur_Sigma( 1, : ) = [ Sigma( b, b ) Sigma( b, c ) ];
    Cur_Sigma( :, 1 ) = [ Sigma( b, b ); Sigma( c, b ) ];
    %[ bb bc
    %  cb cc ]
    det_bc_bc = det( Cur_Sigma );
    
    if det_ac_ac > 0 && det_bc_bc > 0
        Cur_Sigma( 1, : ) = [ Sigma( b, a ) Sigma( b, c ) ];
        Cur_Sigma( :, 1 ) = [ Sigma( b, a ); Sigma( c, a ) ];
        %[ ba bc
        %  ca cc ]        
        det_bc_ac = det( Cur_Sigma );
    
        pc = det_bc_ac ./ sqrt(  det_ac_ac .* det_bc_bc );
        df = n - length( c ) - 2;
    
        if df == 0
            p = 1;
        elseif abs( pc ) == 1 % df > 0
            p = 0;
        else
            t = pc .* sqrt( df ./ ( 1 - pc .* pc ) );
            p = 1 - tcdf( abs( t ), df );
            p = 2 .* p;
        end
    else
        p = 1;
    end

	if nargin < 6 || isempty( alpha )
        x = p;
    else
        x = p <= alpha;
	end
end % Main Function