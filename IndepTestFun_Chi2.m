function x = IndepTestFun_Chi2( a, b, c, Data, alpha ) % Main function
    % From Main function: Data   
    tb = table( Data( :, [ a, b, c ] ) );
    tb = reshape( tb, size( tb, 1 ), size( tb, 2 ), numel( tb ) ./ size( tb, 1 ) ./ size( tb, 2 ) );

    tb = merge3DConTable_recursive( tb );

    p = testCondIndep_Chi2( tb );
    if nargin < 5 || isempty( alpha )
        x = p;
    else
        x = p <= alpha;
    end
end % Main Function
