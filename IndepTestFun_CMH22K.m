function x = IndepTestFun_CMH22K( a, b, c, Data, alpha ) % Main function
	tb = table( Data( :, [ a, b, c ] ) );
	tb = reshape( tb, size( tb, 1 ), size( tb, 2 ), numel( tb ) ./ size( tb, 1 ) ./ size( tb, 2 ) );

	tb = merge3DConTable_simple( tb, 2 );
        
	p = testCondIndep_CMH22K( tb );
	if nargin < 5 || isempty( alpha )
        x = p;
    else
        x = p <= alpha;
	end
end % Main Function

function t = correct3DConTable( t ) % Sub Function
t = t + 0.5;
end % Sub Function