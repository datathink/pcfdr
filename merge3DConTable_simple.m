function t = merge3DConTable( t, m ) % Main Function
if nargin < 2
    m = 2;
end

s = squeeze( sum( sum( t, 1 ), 2 ) );

idx_l = find( s < m );
idx_g = find( s >= m );

if sum( s( idx_l ) ) >= m || length( idx_g ) == 0
	t = cat( 3, sum( t( :, :, idx_l ), 3 ), t( :, :, idx_g ) );
else
	t = cat( 3, sum( t( :, :, idx_l ), 3 ) + t( :, :, idx_g( 1 ) ), t( :, :, idx_g( 2 : end ) ) );
end


end % Main Function
