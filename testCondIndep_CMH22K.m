function [ p, CMH, df ] = testCMH_22K( t )
% t: a 2-2-K matrix
K = size( t, 3 );
n_x = sum( t, 2 ); % 2-by-1-by-K
n_x = reshape( n_x, 2, K ); % 2-by-K
n_y = sum( t, 1 ); % 1-by-2-by-K
n_y = reshape( n_y, 2, K ); % 2-by-K
n_k = sum( n_x, 1 ); % 1-by-K

u_11k = n_x( 1, : ) .* n_y( 1, : ) ./ n_k; % 1-by-K
v_11k = n_x( 1, : ) .* n_x( 2, : ) .* n_y( 1, : ) .* n_y( 2, : ) ./ n_k ./ n_k ./ ( n_k - 1 ); % 1-by-K

d_11k = sum( reshape( t( 1, 1, : ), 1, K ) - u_11k );

CMH = d_11k .* d_11k ./ sum( v_11k );
df = 1;
p = 1- chi2cdf( CMH, df );

end % Main Function
