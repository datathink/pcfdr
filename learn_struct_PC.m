function G = learn_struct_PC( X, varargin )
% Function: learn_struct_PC
%	Learning the skeleton of Bayesian networks using the PC algorithm, with the False Discovery Rate (FDR) controlled
%
% Syntax:
% 	G = learn_struct_PC( X, ... )
%
% Input:
% 	X: a matrix whose each row is an observation and each column is a variable.
%	...: options, in the format of 'opt_name1', value1, 'opt_name2', value2, ...
%
% Output:
%	G: an undirected graph, encoded as an upper-triangular matrix
%
% Options and their [ Default ] values:
%
% --------- Options related to Error Rate Control Strategy ---------
%
% error_method [ 'FDRheur' ]: the method used to control error rates. Please refer
%           to reference JMLR2009. Supported values are:
%
%           'FDR': control the false discovery rate (FDR) with the PC_fdr algorithm
%           'FDRheur': control the FDR with the PC_fdr* algoithm
%           'Err1': control the type I error rate with the standard PC algorithm
%
% error_rate [ 0.05 ]: the threshold of the error rate of interest.
%            A numerical number between 0 and 1.
%
% fdr_func [ empty ]: the function of calculating q-values from
%           p-values. If it is NOT specified, or empty [], the function will
%           be automatically specified by OPTION fdr_method. If it is specified as a
%           function handle, it must have the interface:
%               q-values = function_name( p-values ),
%           where q-values and p-values are two numerical vectors.
%
%           ** By supplying fdr_func, users can use their customized
%           calcuation of q-values.
%
% fdr_method [ 'benjamini1995' ]: use those q-value functions natively supported by
%           this package. Supported values are:
%
%           'benjamini1995': use the method published in 1995 by Y. Benjamini and Y. Hochberg
%               Please refer to reference [Benjamini1995].
% 
%           When option fdr_func is specfied, this option will BE OVERRIDEN.
%
% --------- Options related to Conditional-Independence Tests ---------
%
% indep_test_func [ empty ]: the function of conditional-independence
%            test. If it is NOT specified, or empty [], the function will
%            be automatically specified by OPTION indep_test_method. If it is specified as a
%            function handle, it must have the interface:
%                   p-value = function_name( a, b, C ),
%            where a and b are the indices of the two nodes of an edge, and C is a vector
%            of the indices of the conditional nodes. For example:
%                   p-value = my_indep_test( 1, 2, [ 3, 5 ] );
%                   % test the independence between node 1 and node 2 given
%                   % nodes 3 and 5
%              or   p-value = my_indep_test( 1, 2, [] );
%                   % test the indpendence between node 1 and node 2.
%
%            ** By supplying indep_test_func, users can use their customized
%            conditional-independence tests. Users can implement their own test function as follows:
%           (1) implement a p-value function func1: p-value = func1( data, a, b, C );
%           (2) define a functional handle with data defaultly set as
%           X and interface adapted to learn_struct_PC:
%				my_indep_test = @( a, b, C ) func1( X, a, b, C );
%				G = learn_struct_PC( X, 'indep_test', my_indep_test );
%           In this case, to miminize data passing between functions, users
%           can also input zeros( 0, size( X, 2 ) ) to learn_struct_PC:
%				G = learn_struct_PC( zeros( 0, size( X, 2 ) ), 'indep_test', my_indep_test );
%           because all the information about X has been wrapped in my_indep_test.
%
% indep_test_method [ 'Gaussian' ]: use those conditional-independence
%           tests natively by this package. When option indep_test_func is
%           specfied, this option will be OVERRIDEN. Supported values are:
%
%           'Gaussian': partial-correlation-coefficient test, for
%                     continuous data
%           'CMH22K': Cochran-Mantel-Haenszel (CMH) test, for binary data
%           'Chi2': chi-square test, for categorical data
%
%
% -------- Options related to Prior Knowledge -------------
%
% init_graph [ a fully connected graph ]: the graph of all the
%           possible edges, i.e. including both those that MUST be in the
%           graph, and those that are UNCERTAIN, but excluding those
%           that definately CANNOT be in the graph.
%
% protected_subgraph [ an empty graph ]: those edges that MUST be in
%           the graph.
%
%       init_graph and protected_subgraph are encoded as logical upper
%       triangluar matrices. protected_subgraph should be a subset of init_graph,
%       If not, extra edges not in init_graph will be REMOVED from protected_subgraph.
%
% -------- Options related to the PC Search Strategy
% depth [ #nodes - 2 ]: the maximum number of conditional variables.
%
%
% Examples:
%
%   % Example 1: Use default options
%   G = learn_struct_PC( X );
%   % Learn graph with default options, as the command below:
%
%	G = learn_struct_PC( X, 'error_method', 'FDRheur', 'error_rate', 0.05, ...
%           'indep_test_method', 'Gaussian', 'fdr_method', 'benjamini1995', 'depth', size( X, 2 ) - 2 );
%
%       % Control the FDR at 0.05 with the PC_fdr* algorithm
%       % Test conditional-independence of continuous data with the test of
%       % partial correlation coeffients.
%       % Calculate q-values with the method in reference Benjamini1995
%
%   % Example 2: Use customized conditional independence test and q-value calculation:
%	G = learn_struct_PC( X, 'error_method', 'FDR', 'error_rate', 0.05, ...
%           'indep_test_func', @my_indep_test, 'fdr_func', @my_fdr_func );
%
%       % Control the FDR at 0.05 with the PC_fdr algorithm
%       % Test conditional-independence of with my_indep_test.
%       % Calculate q-values from p-values with my_fdr_func.
%
%   % Example 3: Use prior knowledge
%   N = size( X, 2 ); % the number of nodes.
%   G_all = triu( true( N, N ), 1 ); G_all( 2, 4 ) = false;
%   G_must = false( N, N ); G_must( 1, 2 ) = true;
%   G = learn_struct_PC( X, 'error_method', 'Err1', 'error_rate', 0.05, ...
%           'init_graph', G_all, 'protected_subgraph', G_must );
%
%       % Control the type I error rate at 0.05 with the standard PC algorithm
%       % node 2 and 4 cannot be connected
%       % node 1 and 2 must be connected
%
%   % Example 4: Control the PC search
%       G = learn_struct_PC( X, 'depth', 3 );
%       % search at most three conditional variables.
%
% References:
%
%   [JMLR2009]
%   Junning Li, Z. Jane Wang (2009)
%   "Controlling the False Discovery Rate of the Association/Causality Structure Learned with the PC Algorithm"
%   Journal of Machine Learning Research, Vol. 10, page 475 - 514
%
%   [EMBC2008]
%   Junning Li, Z. Jane Wang and Martin J. McKeown (2008)
%   "Learning brain connectivity with the false-discovery-rate-controlled PC-algorithm"
%   Proceedings of the 30th Annual International Conference of the IEEE
%   Engineering in Medicine and Biology Society. 4617 - 4620
%
%   [Benjamini1995]
%   Y Benjamini, Y Hochberg (1995)
%   "Controlling the false discovery rate: a practical and powerful approach to multiple testing"
%   JOURNAL-ROYAL STATISTICAL SOCIETY SERIES B, Vol. 57, page 289-300
%
%
% Version:
%	2009-03-03: The first release, as used in publications [EMBC2008] and [JMLR2009]
%
% Credit:
%	DataThink
%
% Contact:
%  	datathink@@users.sourceforge.net

N = size( X, 2 );

opt_cfg = {	...
        'error_method', 'FDRheur'; ... or 'FDR', 'Err1'
		'error_rate', 0.05; ...
		'indep_test_func', []; ...
		'indep_test_method', 'Gaussian'; ... or 'CMH22K', 'Chi2'
		'fdr_func', []; ...
        'fdr_method', 'benjamini1995'; ...
      	'init_graph', triu( true( N, N ), 1 ); ...
       	'protected_subgraph', triu( false( N, N ), 1 ); ...
        'depth', N - 2; ...
       	'nextEdge', 'max_pvalue' ... or 'fixed order'
	};

opt = parse_opt( varargin, opt_cfg );

if isempty( opt.indep_test_func )
	opt.indep_test_func = makeCondIndepTestFunc( opt.indep_test_method, X );
end

if isempty( opt.fdr_func )
	switch opt.fdr_method
		case 'benjamini1995'
			opt.fdr_func = ( @( p ) fdr_benjamini1995( p, opt.error_rate ) );
	end
end

switch opt.error_method
    case 'FDR'        
        whichEdgeToRemove = makeEdgeToRemoveFunc( 'FDR', 'indep_test', opt.indep_test_func, 'fdr_func', opt.fdr_func );
    case 'FDRheur'
        whichEdgeToRemove = makeEdgeToRemoveFunc( 'FDRheur', 'indep_test', opt.indep_test_func, 'fdr_func', opt.fdr_func );
    case 'Err1'
        whichEdgeToRemove = makeEdgeToRemoveFunc( 'Err1', 'indep_test', opt.indep_test_func, 'err1', opt.error_rate );
end

G = searchPC( N, whichEdgeToRemove, 'init_graph', opt.init_graph, 'protected_subgraph', opt.protected_subgraph, 'depth', opt.depth, 'nextEdge', opt.nextEdge );

end
