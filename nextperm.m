function [ p2, e ] = nextperm( p1 )
% NEXTPERM: Next Permutation in lexicographical order
%
% Syntax:
%   [ p2, e ] = nextperm( p1 )
%
% Input:
%   p1: the current permutation. a vector. char or double
%
% Output:
%   p2: the next permutation. a vector has the same size as p1.
%
%   e: the flag of the end. If p1 is the last permutation in
%   lexicographical order, e = false; otherwise e = true;
%
% The lexicographical order:
%   Example:
%   abcd -> abdc -> acbd -> acdb ... -> dcba
%
% Please refer to C++ STL next_permutation.